﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour {

    public PlayerMovement movement;

    // ------------------------------------------------------------------------

    private void OnCollisionEnter(Collision collisionInfo)
    {
        // Detect a collision with an obstacle
        if (collisionInfo.collider.tag == "Obstacle")
        {
            // Stop the movement
            //Debug.Log(collisionInfo.collider.name);
            movement.enabled = false;

            FindObjectOfType<GameManager>().EndGame();
        }
        
    }

    // ------------------------------------------------------------------------
}
