﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Rigidbody rb;
    public float forwardForce = 2000f;
    public float sidewaysForce = 500f;

    // ------------------------------------------------------------------------

    // FixUpdate is used because we are using it to mess with physics
    void FixedUpdate () {

        // Add a forward force
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);

        // Add force to the right
        if (Input.GetKey("d"))  
        {
            rb.AddForce(sidewaysForce, 0, 0, ForceMode.VelocityChange);
        }

        // Add force to the left
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sidewaysForce, 0, 0, ForceMode.VelocityChange);
        }

        // the player has fallen from the platform
        if (rb.position.y < -3f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }

    }

    // ------------------------------------------------------------------------
}
